# TimeSelectorJS #

This is a simple library to generate `Time Slots` in HTML pages.

---

# Prerequisites #

**jQuery** - For the rapid development and readablity, Library developed on top of `jQuery`.

**Bootstrap** - We are using `Bootstrap Classes` to style and align the elements.

---

# Usage #


**Add Script**

The `source` folder in this repository contains 2 files, `time-selector.js` and `time-selector.min.js`. You can use any file you want since the `min.js` indicates the minified version.

```html
<script src='/your/asset/path/time-selector.min.js' type='text/javascript'></script>
```

**Create Wrapper**

Once you have added the script, you need to create to load the plugin elements.

```html
<div id='timeSelectorWrapper'></div>
```

**Trigger Plugin**

After creating a wrapper, You need to initiate the plugin.

```javascript
TimeSelector.init("timeSelectorWrapper"); // Use wrapper ID
```

**Showing Values When Render**

When you trigger the plugin, The Time fields will be empty by default. But if you want to set values by default, You can do it using `setTime()` method.

```javascript
TimeSelector.init("timeSelectorWrapper");

// Setting value while render

const timeData = [
   {
     day_id: 1, // 1 indicates Tuesday while 0 is Monday and 6 is Sunday
     starts_at: '23:21:16',
     ends_at: '23:21:16'
   },
   {
     day_id: 4, // 4 indicates Friday while 0 is Monday and 6 is Sunday
     starts_at: '13:21:16',
     ends_at: '13:21:16'
   },
];

TimeSelector.setTimes("timeSelectorWrapper", timeData);
```



