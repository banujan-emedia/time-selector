window["TimeSelector"] = {
    days: ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'],
    init: function setTimeSlotPlugin(targetElement) {
        TimeSelector.days.forEach((v,i) => {
            const htmlContent = `
            <div class="row mx-0">
                <div class="col-sm-2">
                    <label class="font-weight-bold">
                        <input type="checkbox" class="form-check-input" checked>
                        ${v.trim().replace(/^\w/, (c) => c.toUpperCase())}
                    </label>
                </div>
                <div class="col-sm-10 mb-4">
                    <div id="${targetElement}_${v}_time_list"></div>
                    <span style="color: #007bff;cursor:pointer" class="ml-3" onclick="TimeSelector.addTime('${targetElement}_${v}_time_list', '${targetElement}', '${v}')">+ Add Time</span>
                </div>
            </div>
        `;

            $('#'+targetElement).append(htmlContent);
        });
    },
    addTime: function (container, name, day, startsAt = null, endsAt = null) {
        const containerElement = $('#'+container);
        const namePrefix = TimeSelector.generateNameRecursive(name, day, containerElement.children().length);

        containerElement.append(`
            <div class="row mx-0 mb-2">
                <div class="col-sm-5">
                    <input type="time" name="${namePrefix}['start']" value="${startsAt}">
                </div>
                <div class="col-sm-5">
                    <input type="time" name="${namePrefix}['end']" value="${endsAt}">
                </div>
                <div class="col-sm-2">
                    <span style="color: red;cursor: pointer" onclick="TimeSelector.removeTime(this)">Remove</span>
                </div>
            </div>
        `);
    },
    removeTime: function (e) {
        $(e).parent().parent().remove();
    },
    generateNameRecursive: function (name, day, index) {
        const namePrefix = `${name}['${day}'][${index}]`;

        if ( $(`input[name="${namePrefix}['start']"]`).length > 0 ) {
            return TimeSelector.generateNameRecursive(name, day, (index + 1));
        }

        return namePrefix;
    },
    setTimes: function (targetElement, data) {
        data.forEach((v, i) => {
            const container = `${targetElement}_${TimeSelector.days[v['day_id']]}_time_list`;
            TimeSelector.addTime(container, targetElement, TimeSelector.days[v['day_id']], v['starts_at'], v['ends_at']);
        });
    }
};